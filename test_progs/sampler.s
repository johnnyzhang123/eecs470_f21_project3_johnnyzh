.section .text
.align 4
	nop	
	li sp, 2048
## Immediate tests ##
	li t0, 0x1 #TODO: this will be the test number	
	li t6 , 0 #zero out
	ori t6, t6, -2048 
	ori t1, t1, -1 
	li t2, 0
	addi t2, t2, 3
	andi t2, t2, 1
	li t3, 0
	xori t3, t3, -1 
	bne t3, t1, FAIL
	andi t1, t1, -2048
	bne t1,t6, FAIL
	slti t4, t1, 1	
	sltiu t5, t1, 1
	bne t4,t5, im_hop
	bge t1, t1, FAIL
im_hop:	
	li t2, 0x1
	slli t2, t2, 12
	lui t1, 1
	bne t2, t1, FAIL
	lui t1, 0xfffff
	srli t1, t1, 31
	li t2, 1
	bne t2, t1, FAIL
	lui t1, 0xfffff
	srai t1, t1, 31
	lui t2, 0xfffff
	ori t2, t2, -1
	bne t2, t1, FAIL 
## Memory tests ##
	li t0, 0x2 #TODO: testname	
	li t1, 255
	sb t1, 0(sp) 
	lb t2, 0(sp)
	bge t2, t1, FAIL
	lbu t2, 0(sp)
	bne t1,t2, FAIL	
	ori t1, t1, -1 
	lui t1, 0xf	
	sh t1, 0(sp)
	lh t2, 0(sp)
	bge t2, t1, FAIL
	lhu t2, 0(sp)
	bne t2, t2, FAIL	
	ori t1, t1, -1 
	lui t1, 0x7ffff
	sw t1, 0(sp)
	lw t2, 0(sp)
	bne t1, t2, FAIL
## Arithimetic between register tests ##
	li t0, 0x3
	li t1, 5
	li t2, 0
	add t2, t1, t1
	slli t1, t1, 1	
	bne t1, t2, FAIL
	li t1, 0x3
	li t2, 0x4
	or t1, t1,t2
	li t3, 0x7
	bne t1,t3, FAIL	
	li t1, 3
	sub t1, t1, t2
	li t2, -1
	bne t1,t2, FAIL
	# TODO: Finish out with arithmetic instructions, they have been enurmated 
	# in the previous section with immeadiates, just need to test them w/ reg args
## MULT Instructions ##
	li t0, 0x4
	li t1, 14
	li t2, 40
	mul t3, t2, t1
	li t4, 560
	bne t4, t3, FAIL
	lui t1, 0x7ff00
	lui t2, 0x55555
	mulhu t5, t1, t2
	mul t4, t1, t2
	lui t1, 0xfff00
    lui t2, 0xf5555
	mulh t5, t1,t2
	mul t4, t1,t2
	wfi
FAIL: 
	wfi
